cmake_minimum_required(VERSION 3.14)
project(host-thunks)

set(CMAKE_CXX_STANDARD 20)

# Syntax: generate(libxyz libxyz-interface.cpp generator-targets...)
# This defines a target and a custom command:
# - custom command: Main build step that runs the thunk generator on the given interface definition
# - libxyz-${GUEST_ARCH}-host-deps: Interface target to read include directories from which are passed to libclang when parsing the interface definition
function(generate NAME INTERFACE_FILE)
  # Interface target for the user to add include directories
  add_library(${NAME}-${GUEST_ARCH}-host-deps INTERFACE)
  target_include_directories(${NAME}-${GUEST_ARCH}-host-deps INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/../include")
  target_compile_definitions(${NAME}-${GUEST_ARCH}-host-deps INTERFACE HOST_THUNK_LIBRARY)
  # Shorthand for the include directories added after calling this function.
  # This is not evaluated directly, hence directories added after return are still picked up
  set(prop "$<TARGET_PROPERTY:${NAME}-${GUEST_ARCH}-host-deps,INTERFACE_INCLUDE_DIRECTORIES>")
  set(compile_prop "$<TARGET_PROPERTY:${NAME}-${GUEST_ARCH}-host-deps,INTERFACE_COMPILE_DEFINITIONS>")

  # Run thunk generator for each of the given output files
  foreach(WHAT IN LISTS ARGN)
    set(OUTFOLDER "${CMAKE_CURRENT_BINARY_DIR}/gen/${NAME}")
    set(OUTFILE "${OUTFOLDER}/${WHAT}")

    file(MAKE_DIRECTORY "${OUTFOLDER}")

    file(GLOB DEPS
      "${CMAKE_CURRENT_SOURCE_DIR}/../Templates/*"
      "${CMAKE_CURRENT_SOURCE_DIR}/../lib${NAME}/*"
    )
    add_custom_command(
      OUTPUT "${OUTFILE}"
      DEPENDS "${THUNKGEN}"
      DEPENDS "${INTERFACE_FILE}"
      DEPENDS "${DEPS}"
      COMMAND "${THUNKGEN}" "${OUTFOLDER}" "${NAME}" "${INTERFACE_FILE}" "${WHAT}" -- "${CMAKE_CURRENT_SOURCE_DIR}/../Templates" "${CMAKE_CURRENT_SOURCE_DIR}/../lib${NAME}" --
        -std=c++20
        -DGUEST_THUNK_LIBRARY
        -DGUEST_ARCH_${GUEST_ARCH}
        # Expand compile definitions to space-separated list of -D parameters
        "$<$<BOOL:${compile_prop}>:;-D$<JOIN:${compile_prop},;-D>>"
        # Expand include directories to space-separated list of -isystem parameters
        "$<$<BOOL:${prop}>:;-isystem$<JOIN:${prop},;-isystem>>"
      VERBATIM
      COMMAND_EXPAND_LISTS
      )

    list(APPEND OUTPUTS "${OUTFILE}")
  endforeach()
  set(GEN_${NAME} ${OUTPUTS} PARENT_SCOPE)
endfunction()

function(add_host_lib NAME)
  set (SOURCE_FILE ../lib${NAME}/lib${NAME}_Host.cpp)
    get_filename_component(SOURCE_FILE_ABS "${SOURCE_FILE}" ABSOLUTE)
  if (NOT EXISTS "${SOURCE_FILE_ABS}")
    set (SOURCE_FILE ../lib${NAME}/Host.cpp)
    get_filename_component(SOURCE_FILE_ABS "${SOURCE_FILE}" ABSOLUTE)
    if (NOT EXISTS "${SOURCE_FILE_ABS}")
      message (FATAL_ERROR "Thunk source file for Host lib ${NAME} doesn't exist!")
    endif()
  endif()

  add_library(${NAME}-${GUEST_ARCH}-host SHARED ${SOURCE_FILE} ${GEN_lib${NAME}})
  target_include_directories(${NAME}-${GUEST_ARCH}-host PRIVATE "${CMAKE_CURRENT_BINARY_DIR}/gen/lib${NAME}" "${THUNKS_GUEST_GEN_DIR}/lib${NAME}")
  target_compile_definitions(${NAME}-${GUEST_ARCH}-host PRIVATE HOST_THUNK_LIBRARY GUEST_ARCH_${GUEST_ARCH})
  target_link_libraries(${NAME}-${GUEST_ARCH}-host PRIVATE dl lib${NAME}-${GUEST_ARCH}-host-deps)
  ## Make signed overflow well defined 2's complement overflow
  target_compile_options(${NAME}-${GUEST_ARCH}-host PRIVATE -fwrapv -fvisibility=hidden)

  # generated files forward-declare functions that need to be implemented manually, so pass --no-undefined to make sure errors are detected at compile-time rather than runtime
  target_link_options(${NAME}-${GUEST_ARCH}-host PRIVATE "LINKER:--no-undefined")

  install(TARGETS ${NAME}-${GUEST_ARCH}-host DESTINATION ${INSTALL_DIR})
endfunction()

#generate(libEGL ${CMAKE_CURRENT_SOURCE_DIR}/../libEGL/libEGL_interface.cpp function_unpacks tab_function_unpacks ldr ldr_ptrs)
#add_host_lib(EGL)

generate(libGL ${CMAKE_CURRENT_SOURCE_DIR}/../libGL/libGL_interface.cpp function_unpacks.inl tab_function_unpacks.inl ldr.inl ldr_ptrs.inl)
add_host_lib(GL)

find_package(OpenGL REQUIRED)
target_link_libraries(GL-${GUEST_ARCH}-host PRIVATE OpenGL::GL)
target_link_libraries(GL-${GUEST_ARCH}-host PRIVATE X11)
target_link_libraries(GL-${GUEST_ARCH}-host PRIVATE Xmu)

generate(libvulkan ${CMAKE_CURRENT_SOURCE_DIR}/../libvulkan/libvulkan_interface.cpp function_unpacks.inl tab_function_unpacks.inl ldr.inl ldr_ptrs.inl)
target_include_directories(libvulkan-${GUEST_ARCH}-host-deps INTERFACE ${FEX_PROJECT_SOURCE_DIR}/External/Vulkan-Headers/include/)
add_host_lib(vulkan)
target_link_libraries(vulkan-${GUEST_ARCH}-host PRIVATE xcb)
target_link_libraries(vulkan-${GUEST_ARCH}-host PRIVATE X11)
target_link_libraries(vulkan-${GUEST_ARCH}-host PRIVATE Xmu)