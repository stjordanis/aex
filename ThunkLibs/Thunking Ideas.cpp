Future Plans

/*
    Syms are needed for
    - local packers
        fexfn::pack
    - local unpackers
        fexfn::unpack
    - local impl
        fexfn::impl<ArgsRV>

    - guest public symbols
        simple function that calls impl

    - host receptors
        simple function that calls impl

    guest -> host can be done with a GCH

        on init, guest passes a list to be filled by host, and registered with the GCH interface

        guest can just then call directly there for an IR trampoline

    host -> guest
        guest addr can be directly executable
        we'll have to GC thunks, somehow, at some point
        host generated pre-ramble, that injects { guest env, guest unpacker, guest fn }


    preventing host -> guest misfires
        guest compile code needs to honor guest ^PROT_EXEC
    
    preventing guest -> host misfires
        guest memory needs to be ^PROT_EXEC

    automatic switching /can/ be implemented as a (very) ugly thing. Will cost a segfault in
        host -> guest, and guest -> host can be done transparently

        Though, how does one infer arg mappings?

*/
template<bool IsHostcall, typename ArgsRV>
struct fexfn {
    using packed_argsrv_t = ArgsRV;

    template<auto>
    void pack();

    template<typename R, Typename... Args>
    R pack<R(Args...)>(Args ...) {
        uintptr_t host_addr;
        static if (IsHostcall) {
            CUSTOM_ABI_HOST_ADDR;
        }

        packed_argsrv_t argsrv = { Args...};
        packed_argsrv_t::thunk(&argsrv, host_addr);

        if constexpr (!std::is_void_v<Result>) {
            return packed_args.rv;
        }
    }

    template<auto>
    void unpack();

    template<auto F, typename R, Typename... Args>
    void unpack<R(Args...)>(packed_argsrv_t &argsrv, uintptr_t host_addr) {
        R(*fn)(Args...);

        static if (IsHostcall) {
            fn = reinterpret_cast<F*>(host_addr);
        } else {
            fn = &F;
        }

        auto args = argsrv.getArgs();
        if constexpr (!std::is_void_v<Result>) {
            argsrv.rv = { fn(std::get<std::make_index_sequence<sizeof(Args)>>...), argsrv};
        } else {
            fn(std::get<std::make_index_sequence<sizeof(Args)>>...);
            argsrv.rv = { argsrv }
        }
    }
};