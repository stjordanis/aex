{% import "common.tpl" as common %}

extern "C" {
{# guest packers #}
{% for thunk in thunks %}
typed<{{thunk.rv.qual_type}}> fexfn_pack_{{thunk.name}} ({{common.args_str(thunk.args)}}) {

    TRACE_PACK_ENTER("{{libname}}:{{thunk.name}}")

    auto argsrv = thunks::packed_argsrv_t<thunks::packed_argsrv_{{thunk.name}}_t> {
    {%- for arg in thunk.args %}
        {{common.arg_name(arg)}}{{common.append_if(not loop.last, ", ")}}
    {%- endfor %}
    };

    {
        pack_marshal_t marshal {&argsrv};
        fexthunks_{{libname}}_{{thunk.name}}(&argsrv);
    }

    TRACE_PACK_EXIT("{{libname}}:{{thunk.name}}")

    {% if thunk.has_rv -%}
    return argsrv.rv;
    {%- endif %}
}

typed<{{thunk.rv.qual_type}}> fexfn_hostaddr_pack_{{thunk.name}} ({{common.args_str(thunk.args, true)}} uintptr_t host_addr) {

    TRACE_HOSTADDR_PACK_ENTER("{{libname}}:{{thunk.name}}")

    auto argsrv = thunks::packed_argsrv_t<thunks::packed_argsrv_{{thunk.name}}_t> {
    {%- for arg in thunk.args %}
        {{common.arg_name(arg)}}{{common.append_if(not loop.last, ", ")}}
    {%- endfor %}
    };

    {
        pack_marshal_t marshal {&argsrv};
        fexthunks_{{libname}}_{{thunk.name}}_hostaddr(&argsrv, host_addr);
    }

    TRACE_HOSTADDR_PACK_EXIT("{{libname}}:{{thunk.name}}")

    {% if thunk.has_rv -%}
    return argsrv.rv;
    {%- endif %}
}

typed<{{thunk.rv.qual_type}}> fexfn_hostcall_pack_{{thunk.name}} ({{common.args_str(thunk.args)}}) {
    CUSTOM_ABI_HOST_ADDR;

    TRACE_HOSTCALL_PACK_ENTER("{{libname}}:{{thunk.name}}")

    auto argsrv = thunks::packed_argsrv_t<thunks::packed_argsrv_{{thunk.name}}_t> {
    {%- for arg in thunk.args %}
        {{common.arg_name(arg)}}{{common.append_if(not loop.last, ", ")}}
    {%- endfor %}
    };

    {
        pack_marshal_t marshal {&argsrv};
        fexthunks_{{libname}}_{{thunk.name}}_hostaddr(&argsrv, host_addr);
    }

    TRACE_HOSTCALL_PACK_EXIT("{{libname}}:{{thunk.name}}")

    {% if thunk.has_rv -%}
    return argsrv.rv;
    {%- endif %}
}
{% endfor %}
}