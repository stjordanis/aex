{% import "common.tpl" as common %}

{# thunk decls #}

template<typename T> using typed = T;

namespace thunks {
namespace detail {
template<auto> struct gint;

template<> struct gint<1> { using type = int8_t; };
template<> struct gint<2> { using type = int16_t; };
template<> struct gint<4> { using type = int32_t; };
template<> struct gint<8> { using type = int64_t; };

template<auto> struct guint;
template<> struct guint<1> { using type = uint8_t; };
template<> struct guint<2> { using type = uint16_t; };
template<> struct guint<4> { using type = uint32_t; };
template<> struct guint<8> { using type = uint64_t; };

template<auto> struct gflt;
template<> struct gflt<4> { using type = float; };
template<> struct gflt<8> { using type = double; };
}

template<typename V>
struct guest_wrapper {
    V value;
    guest_wrapper() = default;

    template<typename T>
    guest_wrapper(T *v) {
        value = static_cast<decltype(value)>((uintptr_t)v);
    }

    template<typename T>
    guest_wrapper(const T &v) {
        value = static_cast<decltype(value)>(v);
    }

    template<typename T>
    operator T*() {
        return reinterpret_cast<T*>((uintptr_t)value);
    }

    template<typename T>
    operator T() {
        return static_cast<T>(value);
    }
};

template<auto bytes>
struct guest_int: guest_wrapper <typename detail::gint<bytes>::type> { };

template<auto bytes>
struct guest_uint: guest_wrapper <typename detail::guint<bytes>::type> { };

template<auto bytes, typename N>
struct guest_enum: guest_wrapper <typename detail::guint<bytes>::type> { };

template<auto bytes>
struct guest_float: guest_wrapper <typename detail::gflt<bytes>::type> { };

template<auto bytes, typename pointee>
struct guest_ptr: guest_wrapper <typename detail::guint<bytes>::type> {
    using pointee_t = pointee;

    pointee *ptr() { return (pointee *)*this; }

    pointee *operator-> () { return ptr(); }
};

template<auto count, typename item_type>
struct guest_array {
    item_type items[count];
};

template <typename Fn>
struct guest_function { };

template<typename>
union guest_type;

template<typename>
struct guest_incomplete;

// Forward declarations
{% for struct in structs %}
struct {{struct.name}};
{%- endfor %}

{% for enum in enums %}
struct {{enum.name}};   
{%- endfor %}

// Guest Definitions
{% for struct in structs %}
template<>
union guest_type<{{struct.name}}> {
    uint8_t bytes[{{struct.size}}];
    {% for field in struct.fields %}
    struct {
        {% if field.offset != 0 %}uint8_t pad_{{field.name}}[{{field.offset}}];{% endif %}
        {{field.mapped_type}} {{field.name}};
    };
    {% endfor %}
};
{% endfor %}


template<typename> struct packed_argsrv_t;

{% for thunk in thunks %}
    struct packed_argsrv_{{thunk.name}}_t;
    template<>
    struct packed_argsrv_t<packed_argsrv_{{thunk.name}}_t> {
        {%- for arg in thunk.args %}
            {{arg.mapped_type}} {{common.arg_name(arg)}};
        {%- endfor %}
        {% if thunk.has_rv -%}
        {{thunk.rv.mapped_type}} rv;
        {%- endif %}        
    };
{% endfor %}
}

template<typename argsrv> struct pack_marshal_t {
    pack_marshal_t(argsrv *) { }
};

template<typename argsrv> struct unpack_marshal_t {
    unpack_marshal_t(argsrv *) { }
};