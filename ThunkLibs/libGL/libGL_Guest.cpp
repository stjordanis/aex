/*
$info$
tags: thunklibs|GL
desc: Handles glXGetProcAddress
$end_info$
*/

#include <algorithm>
#define GL_GLEXT_PROTOTYPES 1
#define GLX_GLXEXT_PROTOTYPES 1

#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/gl.h>
#include <GL/glext.h>

#undef GL_ARB_viewport_array
#include "glcorearb.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <algorithm>
#include <string_view>
#include <unordered_map>

#include <X11/Xmu/CloseHook.h>

#define tracef(...) // fprintf(stderr, __VA_ARGS__);

#include "common/Guest.h"

#include "thunks.inl"

#include "guest_structs.inl"


typedef void voidFunc();

#define dbgf(...) //fprintf(stderr, __VA_ARGS__)
#define errf(...) fprintf(stderr, __VA_ARGS__)

#define IMPL(Name) fexfn_impl_##Name
#define PACKER(Name) fexfn_pack_##Name

// px11 is enabled when libX11 is not thunked
static const bool px11_Enabled = true ; //!IsLibLoaded("libX11");

static int DisplayCloseCallback(Display *dpy, XPointer data);

static Display *px11Cache = nullptr;

// this can be reworked to be faster when h2g is easy
static void AddCloseCallback(Display *dpy){
	if (px11Cache == dpy) {
		return;
	}
	px11Cache = dpy;
	if (!XmuLookupCloseDisplayHook(dpy, nullptr, &DisplayCloseCallback, nullptr)) {
		XmuAddCloseDisplayHook(dpy, &DisplayCloseCallback, nullptr);
	}
}


// HostVis here is a guest-compitable struct with only screen and visualid filled, and needs to be px11_XFree'd
static XVisualInfo *MapVisualInfoHostToGuest(Display *dpy, XVisualInfo *HostVis);

template<typename T, typename C, int ptr_size>
thunks::guest_ptr<ptr_size, T> MapX11PointerArray(thunks::guest_ptr<ptr_size, T> Host, thunks::guest_ptr<ptr_size, C> CountPtr);

#define MARSHAL_GEN(ENTRY, BEFORE, AFTER) \
template<> \
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>; \
	argsrv_t *const argsrv; \
	Display *const GuestDisplay; /* Needed as host will modify argsrv->a_0 */\
    pack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv), GuestDisplay(argsrv->a_0) { BEFORE } \
	~pack_marshal_t() { AFTER } \
};

#define Display_a_0 { AddCloseCallback(GuestDisplay); XSync(GuestDisplay, False); }

#define RV_XVisualInfo { argsrv->rv.value = (uintptr_t)MapVisualInfoHostToGuest(GuestDisplay, argsrv->rv); }
#define RV_PointerMap(pitems) { argsrv->rv = MapX11PointerArray(argsrv->rv, argsrv->pitems); }


MARSHAL_GEN(glXChooseVisual, Display_a_0, RV_XVisualInfo)
MARSHAL_GEN(glXCreateContext, Display_a_0, {}) // Host side XVisualInfo a_1
MARSHAL_GEN(glXDestroyContext, Display_a_0, {})
MARSHAL_GEN(glXMakeCurrent, Display_a_0, {})
MARSHAL_GEN(glXCopyContext, Display_a_0, {})
MARSHAL_GEN(glXSwapBuffers, Display_a_0, {})
MARSHAL_GEN(glXCreateGLXPixmap, Display_a_0, {})  // Host side XVisualInfo a_1
MARSHAL_GEN(glXDestroyGLXPixmap, Display_a_0, {})
MARSHAL_GEN(glXQueryExtension, Display_a_0, {})
MARSHAL_GEN(glXQueryVersion, Display_a_0, {})
MARSHAL_GEN(glXIsDirect, Display_a_0, {})
MARSHAL_GEN(glXGetConfig, Display_a_0, {})  // Host side XVisualInfo a_1

/*GLX 1.1 and later */
MARSHAL_GEN(glXQueryExtensionsString, Display_a_0, {})
MARSHAL_GEN(glXQueryServerString, Display_a_0, {})
MARSHAL_GEN(glXGetClientString, Display_a_0, {})

/*GLX 1.2 and later */
//MARSHAL_GEN(glXGetCurrentDisplay, {}, {}) // Host side Display rv

/*GLX 1.3 and later */
MARSHAL_GEN(glXChooseFBConfig, Display_a_0, RV_PointerMap(a_3))
MARSHAL_GEN(glXGetFBConfigAttrib, Display_a_0, {})
MARSHAL_GEN(glXGetFBConfigs, Display_a_0, RV_PointerMap(a_2))
MARSHAL_GEN(glXGetVisualFromFBConfig, Display_a_0, RV_XVisualInfo)
MARSHAL_GEN(glXCreateWindow, Display_a_0, {})
MARSHAL_GEN(glXDestroyWindow, Display_a_0, {})
MARSHAL_GEN(glXCreatePixmap, Display_a_0, {})
MARSHAL_GEN(glXDestroyPixmap, Display_a_0, {})
MARSHAL_GEN(glXCreatePbuffer, Display_a_0, {})
MARSHAL_GEN(glXDestroyPbuffer, Display_a_0, {})
MARSHAL_GEN(glXQueryDrawable, Display_a_0, {})
MARSHAL_GEN(glXCreateNewContext, Display_a_0, {})
MARSHAL_GEN(glXMakeContextCurrent, Display_a_0, {})
//MARSHAL_GEN(glXGetCurrentReadDrawable, {}, {}); // Maybe needs sync?

MARSHAL_GEN(glXQueryContext, Display_a_0, {})
MARSHAL_GEN(glXSelectEvent, Display_a_0, {})
MARSHAL_GEN(glXGetSelectedEvent, Display_a_0, {})

/*GLX 1.4 and later */
// FEX_TODO("what about plain glXCreateContextAttribs ?")
MARSHAL_GEN(glXCreateContextAttribsARB, Display_a_0, {})
MARSHAL_GEN(glXSwapIntervalEXT, Display_a_0, {})
MARSHAL_GEN(glXQueryRendererIntegerMESA, Display_a_0, {})
MARSHAL_GEN(glXQueryRendererStringMESA, Display_a_0, {})

// Other extensions
MARSHAL_GEN(glXImportContextEXT, Display_a_0, {})
MARSHAL_GEN(glXCopySubBufferMESA, Display_a_0, {})
MARSHAL_GEN(glXMakeCurrentReadSGI, Display_a_0, {})
MARSHAL_GEN(glXGetSyncValuesOML, Display_a_0, {})
MARSHAL_GEN(glXGetMscRateOML, Display_a_0, {})
MARSHAL_GEN(glXSwapBuffersMscOML, Display_a_0, {})
MARSHAL_GEN(glXWaitForMscOML, Display_a_0, {})
MARSHAL_GEN(glXWaitForSbcOML, Display_a_0, {})
MARSHAL_GEN(glXQueryContextInfoEXT, Display_a_0, {})
MARSHAL_GEN(glXBindTexImageEXT, Display_a_0, {})
MARSHAL_GEN(glXReleaseTexImageEXT, Display_a_0, {})
MARSHAL_GEN(glXFreeContextEXT, Display_a_0, {})
MARSHAL_GEN(glXCreateGLXPbufferSGIX, Display_a_0, {})
MARSHAL_GEN(glXDestroyGLXPbufferSGIX, Display_a_0, {})
MARSHAL_GEN(glXQueryGLXPbufferSGIX, Display_a_0, {})
MARSHAL_GEN(glXSelectEventSGIX, Display_a_0, {})
MARSHAL_GEN(glXGetSelectedEventSGIX, Display_a_0, {})
MARSHAL_GEN(glXGetVisualFromFBConfigSGIX, Display_a_0, {})
MARSHAL_GEN(glXCreateContextWithConfigSGIX, Display_a_0, {})
MARSHAL_GEN(glXChooseFBConfigSGIX, Display_a_0, RV_PointerMap(a_3))
MARSHAL_GEN(glXGetFBConfigFromVisualSGIX, Display_a_0, {}) // Host side XVisualInfo a_1
MARSHAL_GEN(glXCreateGLXPixmapWithConfigSGIX, Display_a_0, {})
MARSHAL_GEN(glXGetFBConfigAttribSGIX, Display_a_0, {})

#define MARSHAL_GEN2(ENTRY, BEFORE, AFTER) \
template <> \
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>; \
	argsrv_t *const argsrv; \
    pack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { BEFORE } \
	~pack_marshal_t() { AFTER } \
};

MARSHAL_GEN2(glDebugMessageCallbackARB, { argsrv->a_0.value = 0; }, {})
MARSHAL_GEN2(glDebugMessageCallbackAMD, { argsrv->a_0.value = 0; }, {})
MARSHAL_GEN2(glDebugMessageCallback, { argsrv->a_0.value = 0; }, {})

/*
	glx also exports __glXGLLoadGLXFunction and 
	__GLXGL_CORE_FUNCTIONS (data symbol)
*/

#include "function_packs.inl"
#include "function_packs_public.inl"

int DisplayCloseCallback(Display *dpy, XPointer data) {
    assert(px11_Enabled);
	PACKER(px11_RemoveGuestX11)(dpy);
	return 0;
}

XVisualInfo *MapVisualInfoHostToGuest(Display *dpy, XVisualInfo *HostVis) {
	if (!dpy || !HostVis) { 
		dbgf("MapVisualInfoHostToGuest: Can't map null HostVis, dpy=%p HostVis=%p\n", dpy, HostVis);
		return nullptr; 
	}

    if (!px11_Enabled) {
        return HostVis;
    }

	XVisualInfo v;

	v.screen = HostVis->screen;
	v.visualid = HostVis->visualid;

	PACKER(px11_XFree)(HostVis);

	int c;
	auto vguest = XGetVisualInfo(dpy, VisualScreenMask | VisualIDMask, &v, &c);

	if (c >= 1 && vguest != nullptr) {
		return vguest;
	} else {
		errf("MapVisualInfoHostToGuest: Guest XGetVisualInfo returned null, dpy=%p screen=%d visuald=%lu\n", dpy, v.screen, v.visualid);
		return nullptr;
	}
}

template<typename T, typename C, int ptr_size>
thunks::guest_ptr<ptr_size, T> MapX11PointerArray(thunks::guest_ptr<ptr_size, T> Host, thunks::guest_ptr<ptr_size, C> CountPtr) {
	if (Host.value == 0) {
		return { 0 };
	}

	if (CountPtr.ptr() == nullptr) {
		errf("PointerMap(Host=%p Count_ptr=%p), returning nullptr\n", Host.ptr(), CountPtr.ptr());
		PACKER(px11_XFree)(Host);
		return { 0 };
	}

	long unsigned Count = CountPtr.ptr()->value;

	if (Count == 0) {
		errf("PointerMap(Host=%p Count=%lu), returning nullptr\n", Host.ptr(), Count);
		PACKER(px11_XFree)(Host);
		return { 0 };
	}

	auto rv = (T *)Xmalloc(sizeof(T) * Count);

	if (rv == nullptr) {
		errf("PointerMap(Host=%p Count=%lu): Xmalloc failed, returning nullptr\n", Host.ptr(), Count);
		PACKER(px11_XFree)(Host);
		return { 0 };
	}

	// Host pointers are always 64 bits
	auto Host64bit = reinterpret_cast<uint64_t*>(Host.ptr());

	for (unsigned i = 0; i < Count; i++) {
		rv[i] = T {Host64bit[i]};
	}
	PACKER(px11_XFree)(Host);
	return {rv};
}

#if 0
namespace glx {


// this is needed on the host side
static XVisualInfo *MapVisualInfoGuestToHost(Display *dpy, XVisualInfo *GuestVis) {
	if (!dpy || !GuestVis) { return nullptr; }

	// FEX_TODO("Implement this")
	dbgf("MapVisualInfoGuestToHost %p, %p\n", dpy, GuestVis);
	return PACKER(px11_XVisual)(dpy, GuestVis->screen, GuestVis->visualid);
}

// only on return value
static GLXFBConfig *MapGLXFBConfigHostToGuest(GLXFBConfig *Host, int count) {
	if (!Host) { return nullptr; }

    if (!px11_Enabled) {
        return Host;
    }

	if (!Host || count <= 0) {
		dbgf("MapGLXFBConfigHostToGuest: Host (%p) is null or count (%d) <= 0\n", Host, count);
		return nullptr;
	}

	auto rv = (GLXFBConfig *)Xmalloc(sizeof(GLXFBConfig) *count);

	if (!rv) {
		dbgf("MapGLXFBConfigHostToGuest: Xmalloc failed\n");
		return nullptr;
	}
	
	/*
	for (int i = 0; i < count; i++) {
		rv[i] = Host[i];
	}
	*/


	for (int i = 0; i < count; i++) {
		if constexpr (sizeof(GLXFBConfig) == 4) {
			rv[i] = Host[i * 2];
		}
		else
			rv[i] = Host[i];
	}

	PACKER(px11_XFree)(Host);

	return rv;
}

// only on return value
static GLXFBConfigSGIX *MapGLXFBConfigSGIXHostToGuest(GLXFBConfigSGIX *Host, int count) {
	if (!Host) { return nullptr; }

    if (!px11_Enabled) {
        return Host;
    }

	if (!Host || count <= 0) {
		dbgf("MapGLXFBConfigSGIXHostToGuest: Host (%p) is null or count (%d) <= 0\n", Host, count);
		return nullptr;
	}

	auto rv = (GLXFBConfigSGIX *)Xmalloc(sizeof(GLXFBConfigSGIX) *count);

	if (!rv) {
		dbgf("MapGLXFBConfigSGIXHostToGuest: Xmalloc failed\n");
		return nullptr;
	}

	/*
	for (int i = 0; i < count; i++) {
		rv[i] = Host[i];
	}
	*/


	for (int i = 0; i < count; i++) {
		if constexpr (sizeof(GLXFBConfigSGIX) == 4)
			rv[i] = Host[i * 2];
		else
			rv[i] = Host[i];
	}

	PACKER(px11_XFree)(Host);

	return rv;
}

}

#endif

extern "C" {
	voidFunc *IMPL(glXGetProcAddressARB)(const GLubyte *procname);
	// This is a mesa-only extension as GLX 1.4 has not been ratified
	voidFunc *glXGetProcAddress(const GLubyte *procname) {
		return IMPL(glXGetProcAddressARB)(procname);
	}
}

#include "symbol_list.inl"

extern "C" {
	voidFunc *IMPL(glXGetProcAddressARB)(const GLubyte *procname) {
	auto Ret = PACKER(glXGetProcAddressARB)(procname);
	if (!Ret) {
		return nullptr;
	}

	std::string_view procname_s{reinterpret_cast<const char *>(procname)};

	auto Begin = std::begin(HostPtrInvokers);
	auto End = std::end(HostPtrInvokers);

	auto Invoker = std::lower_bound(Begin, End, procname_s);

	if (Invoker == End || Invoker->name != procname_s) {
		// If glXGetProcAddress is querying itself, then we can just return itself.
		// Some games do this for unknown reasons.
		if (procname_s == "glXGetProcAddress" ||
			procname_s == "glXGetProcAddressARB") {
		return reinterpret_cast<voidFunc *>(glXGetProcAddress);
		}

		// glXGetProcAddress may return pointers for not implemented functions
		// so this should not be fatal
		errf("glXGetProcAddress: packer not found %s\n", procname);
		//__builtin_trap();
		return nullptr;
	}

	LinkAddressToFunction((uintptr_t)Ret, (uintptr_t)Invoker->hostcall);
	//return (voidFunc *)Invoker->impl;
	return Ret;
	}
}

LOAD_LIB(libGL)
