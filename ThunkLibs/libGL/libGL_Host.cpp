/*
$info$
tags: thunklibs|GL
desc: Uses glXGetProcAddress instead of dlsym
$end_info$
*/

#include <cstdio>
#include <unordered_map>
#include <mutex>
#include <shared_mutex>
#include <vector>

#include <dlfcn.h>

#define GL_GLEXT_PROTOTYPES 1
#define GLX_GLXEXT_PROTOTYPES 1

#include "glcorearb.h"

#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include <cstdlib>

#define tracef(...) // fprintf(stderr, __VA_ARGS__);

#include "common/Host.h"

#include <X11/Xmu/CloseHook.h>

#define IMPL(Name) fexfn_impl_libGL_##Name

#define dbgf(...) //fprintf(stderr, __VA_ARGS__)
#define errf(...) fprintf(stderr, __VA_ARGS__)

#include "guest_structs.inl"
#include "ldr_ptrs.inl"

ldr_ptrs_t ldr_ptrs;

namespace glx {

static std::shared_mutex DisplayMapLock;
static std::unordered_map<thunks::guest_type<thunks::_XDisplay> *, Display *> GuestToHost;
static std::unordered_map<Display *, thunks::guest_type<thunks::_XDisplay> *> HostToGuest;

static Display *px11_MapGuestX11(thunks::guest_type<thunks::_XDisplay> *Guest) {
    if (!Guest) { return nullptr; }

    {
        std::shared_lock lk(DisplayMapLock);
        auto rv = GuestToHost.find(Guest);

        if (rv != GuestToHost.end()) {
            return rv->second;
        }
    }

    std::unique_lock lk(DisplayMapLock);

    auto DisplayName = (const char *)Guest->display_name.ptr();
    auto Host = XOpenDisplay(DisplayName);
    if (Host) {
        dbgf("fgl: Mapping Guest Display %p ('%s') to Host %p\n", Guest, DisplayName, Host);
        GuestToHost[Guest] = Host;
        HostToGuest[Host] = Guest;
        return Host;
    } else {
        dbgf("fgl: Failed to open Guest Display %p ('%s') on Host\n", Guest, DisplayName);
        return nullptr;
    }
}

static void IMPL(px11_RemoveGuestX11)(Display *GuestH) {
    auto Guest = (thunks::guest_type<thunks::_XDisplay>*)GuestH;
    if (!Guest) return;

    std::unique_lock lk(DisplayMapLock);
    auto Host = GuestToHost.find(Guest);

    if (Host != GuestToHost.end()) {
        XCloseDisplay(Host->second);
        HostToGuest.erase(Host->second);
        GuestToHost.erase(Host);
    }
}

static thunks::guest_type<thunks::_XDisplay> *px11_HostToGuestX11(thunks::guest_type<thunks::_XDisplay> *HostG) {
    auto Host = (Display*)HostG;
    std::shared_lock lk(DisplayMapLock);
    auto Guest = HostToGuest.find(Host);

    if (Guest != HostToGuest.end()) {
        return Guest->second;
    } else {
        return nullptr;
    }
}

static void IMPL(px11_XFree)(void *p) {
    XFree(p);
}

static XVisualInfo *HostVisual(Display *Host, int screen, unsigned int XVisual) {
    if (!Host) { return nullptr; }


	dbgf("HostVisual: %d, %u\n", screen, XVisual);

    XVisualInfo v;

	v.screen = screen;
	v.visualid = XVisual;

	//dbgf("%d, %lu\n", v.screen, v.visualid);

	int c;
	auto vguest = XGetVisualInfo(Host, VisualScreenMask | VisualIDMask, &v, &c);

	if (c >= 1 && vguest != nullptr) {
		return vguest;
	} else {
		errf("HostVisual: Guest XGetVisualInfo returned null\n");
		return nullptr;
	}
}


template<typename T>
static void MapXVisualInfo(T FrankensteinVisual) {
    if (FrankensteinVisual.value == 0) {
        return;
    }
    auto HostVisal = (XVisualInfo*)FrankensteinVisual;
    auto screen = HostVisal->screen;
    auto visualid = HostVisal->visualid;

    FrankensteinVisual.ptr()->screen.value = screen;
    FrankensteinVisual.ptr()->visualid.value = visualid;
}
}

static void* symbolFromGlXGetProcAddr(void*, const char* name) {
    return (void*)glXGetProcAddress((const GLubyte*)name);
}

#define MARSHAL_GEN(ENTRY, BEFORE, AFTER) \
template<> \
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>; \
	argsrv_t *const argsrv; \
    unpack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { BEFORE } \
	~unpack_marshal_t() { AFTER } \
};

using namespace glx;


#define Display_a_0 { argsrv->a_0.value = (uintptr_t)px11_MapGuestX11(argsrv->a_0); }

#define Flush { XFlush(argsrv->a_0); }
#define XVisualInfo_a_1 { argsrv->a_1 = { HostVisual(argsrv->a_0, argsrv->a_1.ptr()->screen, argsrv->a_1.ptr()->visualid) }; }
#define RV_Display { argsrv->rv.value = (uintptr_t) px11_HostToGuestX11(argsrv->rv.ptr()); }
#define RV_XVisualInfo { MapXVisualInfo(argsrv->rv); }

MARSHAL_GEN(glXChooseVisual, Display_a_0, { RV_XVisualInfo Flush })
MARSHAL_GEN(glXCreateContext, { Display_a_0 XVisualInfo_a_1 }, Flush) // Host side XVisualInfo a_1
MARSHAL_GEN(glXDestroyContext, Display_a_0, Flush)
MARSHAL_GEN(glXMakeCurrent, Display_a_0, Flush)
MARSHAL_GEN(glXCopyContext, Display_a_0, Flush)
MARSHAL_GEN(glXSwapBuffers, Display_a_0, Flush)
MARSHAL_GEN(glXCreateGLXPixmap, { Display_a_0 XVisualInfo_a_1 }, Flush)  // Host side XVisualInfo a_1
MARSHAL_GEN(glXDestroyGLXPixmap, Display_a_0, Flush)
MARSHAL_GEN(glXQueryExtension, Display_a_0, Flush)
MARSHAL_GEN(glXQueryVersion, Display_a_0, Flush)
MARSHAL_GEN(glXIsDirect, Display_a_0, Flush)
MARSHAL_GEN(glXGetConfig, { Display_a_0 XVisualInfo_a_1 }, Flush)  // Host side XVisualInfo a_1

/*GLX 1.1 and later */
MARSHAL_GEN(glXQueryExtensionsString, Display_a_0, Flush)
MARSHAL_GEN(glXQueryServerString, Display_a_0, Flush)
MARSHAL_GEN(glXGetClientString, Display_a_0, Flush)

/*GLX 1.2 and later */
MARSHAL_GEN(glXGetCurrentDisplay, {}, RV_Display) // Host side Display rv

/*GLX 1.3 and later */
MARSHAL_GEN(glXChooseFBConfig, Display_a_0, Flush)
MARSHAL_GEN(glXGetFBConfigAttrib, Display_a_0, Flush)
MARSHAL_GEN(glXGetFBConfigs, Display_a_0, Flush)
MARSHAL_GEN(glXGetVisualFromFBConfig, Display_a_0, { RV_XVisualInfo Flush})
MARSHAL_GEN(glXCreateWindow, Display_a_0, Flush)
MARSHAL_GEN(glXDestroyWindow, Display_a_0, Flush)
MARSHAL_GEN(glXCreatePixmap, Display_a_0, Flush)
MARSHAL_GEN(glXDestroyPixmap, Display_a_0, Flush)
MARSHAL_GEN(glXCreatePbuffer, Display_a_0, Flush)
MARSHAL_GEN(glXDestroyPbuffer, Display_a_0, Flush)
MARSHAL_GEN(glXQueryDrawable, Display_a_0, Flush)
MARSHAL_GEN(glXCreateNewContext, Display_a_0, Flush)
MARSHAL_GEN(glXMakeContextCurrent, Display_a_0, Flush)
//MARSHAL_GEN(glXGetCurrentReadDrawable, {}, {}); // Maybe needs sync?

MARSHAL_GEN(glXQueryContext, Display_a_0, Flush)
MARSHAL_GEN(glXSelectEvent, Display_a_0, Flush)
MARSHAL_GEN(glXGetSelectedEvent, Display_a_0, Flush)

/*GLX 1.4 and later */
// FEX_TODO("what about plain glXCreateContextAttribs ?")
MARSHAL_GEN(glXCreateContextAttribsARB, Display_a_0, Flush)
MARSHAL_GEN(glXSwapIntervalEXT, Display_a_0, Flush)
MARSHAL_GEN(glXQueryRendererIntegerMESA, Display_a_0, Flush)
MARSHAL_GEN(glXQueryRendererStringMESA, Display_a_0, Flush)

// Other extensions
MARSHAL_GEN(glXImportContextEXT, Display_a_0, Flush)
MARSHAL_GEN(glXCopySubBufferMESA, Display_a_0, Flush)
MARSHAL_GEN(glXMakeCurrentReadSGI, Display_a_0, Flush)
MARSHAL_GEN(glXGetSyncValuesOML, Display_a_0, Flush)
MARSHAL_GEN(glXGetMscRateOML, Display_a_0, Flush)
MARSHAL_GEN(glXSwapBuffersMscOML, Display_a_0, Flush)
MARSHAL_GEN(glXWaitForMscOML, Display_a_0, Flush)
MARSHAL_GEN(glXWaitForSbcOML, Display_a_0, Flush)
MARSHAL_GEN(glXQueryContextInfoEXT, Display_a_0, Flush)
MARSHAL_GEN(glXBindTexImageEXT, Display_a_0, Flush)
MARSHAL_GEN(glXReleaseTexImageEXT, Display_a_0, Flush)
MARSHAL_GEN(glXFreeContextEXT, Display_a_0, Flush)
MARSHAL_GEN(glXCreateGLXPbufferSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXDestroyGLXPbufferSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXQueryGLXPbufferSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXSelectEventSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXGetSelectedEventSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXGetVisualFromFBConfigSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXCreateContextWithConfigSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXChooseFBConfigSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXGetFBConfigFromVisualSGIX, { Display_a_0 XVisualInfo_a_1 }, Flush) // Host side XVisualInfo a_1
MARSHAL_GEN(glXCreateGLXPixmapWithConfigSGIX, Display_a_0, Flush)
MARSHAL_GEN(glXGetFBConfigAttribSGIX, Display_a_0, Flush)


#if defined(GUEST_ARCH_x86_32)
#define MARSHAL_SHADDER(ENTRY) \
template<> \
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>; \
	argsrv_t *const argsrv; \
    std::vector<const char *> srcs; \
    unpack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { \
        srcs.resize(argsrv->a_1); \
        for (GLsizei i = 0; i < argsrv->a_1.value; i++) { \
            srcs[i] = (const char *)argsrv->a_2.ptr()[i].ptr(); \
        } \
        argsrv->a_2 = { &srcs[0] }; \
        tracef("unpac_marshal_: %p %ul\n", argsrv->a_2.ptr(), argsrv->a_1.value); \
    } \
};

MARSHAL_SHADDER(glShaderSourceARB)
MARSHAL_SHADDER(glShaderSource)
#endif

#include "function_unpacks.inl"

static ExportEntry exports[] = {
    #include "tab_function_unpacks.inl"
    { {}, nullptr }
};

#include "ldr.inl"

void fill_px11() {
    #define LD(N) \
        ldr_ptrs.fexldr_ptr_libGL_##N = &IMPL(N);

    LD(px11_RemoveGuestX11)
    LD(px11_XFree)
}

EXPORTS(libGL, fill_px11();)
