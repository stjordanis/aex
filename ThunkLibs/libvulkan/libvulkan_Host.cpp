/*
$info$
tags: thunklibs|Vulkan
$end_info$
*/

#define VK_USE_PLATFORM_XLIB_XRANDR_EXT
#define VK_USE_PLATFORM_XLIB_KHR
#define VK_USE_PLATFORM_XCB_KHR
#define VK_USE_PLATFORM_WAYLAND_KHR
#include <vulkan/vulkan.h>
#include <xcb/xcb.h>

#define tracef(...) // fprintf(stderr, __VA_ARGS__);
#define marshalf(...) fprintf(stderr, __VA_ARGS__);

#include "common/Host.h"

#include <cassert>
#include <cstring>
#include <map>
#include <shared_mutex>

#include <dlfcn.h>

#include <X11/Xmu/CloseHook.h> // for Display * definition

#include "guest_structs.inl"
// TODO: fix guest_incomplete

static std::shared_mutex xcb_lock;
static std::map<thunks::guest_incomplete<xcb_connection_t> *, xcb_connection_t*> xcb_GuestToHost;

static xcb_connection_t *XcbGuestToHost(thunks::guest_incomplete<xcb_connection_t> *guest) {
  {
    std::shared_lock lk(xcb_lock);
    auto It = xcb_GuestToHost.find(guest);
    if (It != xcb_GuestToHost.end()) {
      return It->second;
    }
  }

  {
    std::unique_lock lk(xcb_lock);
    auto connection = xcb_connect(nullptr, nullptr);
    if (connection) {
      xcb_GuestToHost[guest] = connection;
    }
    return connection;
  }
}


static std::shared_mutex xlib_lock;
static std::map<thunks::guest_type<thunks::_XDisplay> *, Display*> xlib_GuestToHost;


static Display *XlibGuestToHost(thunks::guest_type<thunks::_XDisplay> *guest) {
  {
    std::shared_lock lk(xlib_lock);
    auto It = xlib_GuestToHost.find(guest);
    if (It != xlib_GuestToHost.end()) {
      return It->second;
    }
  }

  {
    std::unique_lock lk(xlib_lock);
    auto display = XOpenDisplay((const char*)guest->display_name.ptr());
    if (display) {
      xlib_GuestToHost[guest] = display;
    }
    return display;
  }
}


// xcb sleight of connection
// happily opens $DISPLAY, not the matching connection
// and then leaks
template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateXcbSurfaceKHR_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateXcbSurfaceKHR_t>;
    decltype(((argsrv_t*)nullptr)->a_1)::pointee_t CreateInfo;
    unpack_marshal_t(argsrv_t *argsrv) {
        CreateInfo = *argsrv->a_1.ptr();
        // This is yet another ugly hack
        CreateInfo.connection.value = (uintptr_t)XcbGuestToHost(CreateInfo.connection.ptr());
        argsrv->a_1.value = (uintptr_t)&CreateInfo;
        marshalf("vkCreateXcbSurfaceKHR_t: xcb switzeroo\n");
    }
};

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetPhysicalDeviceXcbPresentationSupportKHR_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetPhysicalDeviceXcbPresentationSupportKHR_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = (uintptr_t)XcbGuestToHost(argsrv->a_2.ptr());
        marshalf("XcbPresentationSupportKHR_t: xcb switzeroo\n");
    }
};

// xcb sleight of connection
// happily opens $DISPLAY, not the matching connection
// and then leaks
template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateXlibSurfaceKHR_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateXlibSurfaceKHR_t>;
    decltype(((argsrv_t*)nullptr)->a_1)::pointee_t CreateInfo;
    unpack_marshal_t(argsrv_t *argsrv) {
        CreateInfo = *argsrv->a_1.ptr();
        // This is yet another ugly hack
        CreateInfo.dpy.value = (uintptr_t)XlibGuestToHost(CreateInfo.dpy.ptr());
        argsrv->a_1.value = (uintptr_t)&CreateInfo;
        marshalf("vkCreateXcbSurfaceKHR_t: XOpenDisplay switzeroo\n");
    }
};

#define XLIB_ARG(ENTRY, ARG) \
template<> \
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>; \
    unpack_marshal_t(argsrv_t *argsrv) { \
        argsrv->a_1.value = (uintptr_t)XlibGuestToHost(argsrv->ARG.ptr()); \
        marshalf(#ENTRY ": xcb switzeroo\n"); \
    } \
};

XLIB_ARG(vkAcquireXlibDisplayEXT, a_1)
XLIB_ARG(vkGetPhysicalDeviceXlibPresentationSupportKHR, a_2)
XLIB_ARG(vkGetRandROutputDisplayEXT, a_1)

static VkBool32 DummyVkDebugReportCallback(VkDebugReportFlagsEXT, VkDebugReportObjectTypeEXT, uint64_t, size_t,
                                           int32_t, const char*, const char*, void*) {
  return VK_FALSE;
}

#pragma region Functions with callbacks are overridden to ignore the guest-side callbacks

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateDebugReportCallbackEXT_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateDebugReportCallbackEXT_t>;
    decltype(((argsrv_t*)nullptr)->a_1)::pointee_t CreateInfo;
    unpack_marshal_t(argsrv_t *argsrv) {
        CreateInfo = *argsrv->a_1.ptr();
        CreateInfo.pfnCallback.value = (uintptr_t)&DummyVkDebugReportCallback;
        
        argsrv->a_1.value = (uintptr_t)&CreateInfo;
        argsrv->a_2.value = 0;
    }
};

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateShaderModule_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateShaderModule_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = 0;
    }
};

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateDevice_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateDevice_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = 0;
    }
};

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkAllocateMemory_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkAllocateMemory_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = 0;
    }
};

template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkFreeMemory_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkFreeMemory_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = 0;
    }
};


template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkDestroyDebugReportCallbackEXT_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkDestroyDebugReportCallbackEXT_t>;
    unpack_marshal_t(argsrv_t *argsrv) {
        argsrv->a_2.value = 0;
    }
};


template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateInstance_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateInstance_t>;
    unpack_marshal_t(argsrv_t *argsrv) {

      thunks::guest_type<thunks::VkInstanceCreateInfo> *vk_struct = argsrv->a_0.ptr();

      // assert not the first one, just to be sure
      assert(vk_struct->sType.value != VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT);

      while (vk_struct && vk_struct->pNext.value) {
        auto next = (decltype(vk_struct))vk_struct->pNext.ptr();

        if (next->sType.value == VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT) {
          // Overwrite the pNext pointer, ignoring its const-qualifier (/may fail if in .text/, modifies guest memory in an unexpected way)
          vk_struct->pNext.value = next->pNext.value;
        }

        vk_struct = (decltype(vk_struct))vk_struct->pNext.ptr();
      }

      argsrv->a_1.value = 0;
    }
};
#pragma endregion

#pragma region Disable Wayland extension
// some applications (eg, vulkaninfo) bail out if VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME is not available - even if wayland isn't available
// So instead, it's simpler to thunk wayland-client to always refuse connections, 
// and disable vkGetPhysicalDeviceWaylandPresentationSupportKHR, vkCreateWaylandSurfaceKHR

// THIS MIGHT CRASH
template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetPhysicalDeviceWaylandPresentationSupportKHR_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetPhysicalDeviceWaylandPresentationSupportKHR_t>;
    argsrv_t *argsrv;
    unpack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) {
      marshalf("vkGetPhysicalDeviceWaylandPresentationSupportKHR: calling, will ignore return, hopefully no crash\n");
    }

    ~unpack_marshal_t() {
      argsrv->rv.value = VK_FALSE;
    }
};

// THIS MIGHT CRASH
// THIS IS LEAKY
template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateWaylandSurfaceKHR_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateWaylandSurfaceKHR_t>;
    argsrv_t *argsrv;
    unpack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) {
      marshalf("vkCreateWaylandSurfaceKHR: calling, will ignore return, hopefully no crash\n");
    }

    ~unpack_marshal_t() {
      argsrv->rv.value = VK_ERROR_UNKNOWN;
    }
};


#if 0
template<>
struct unpack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkEnumerateInstanceExtensionProperties_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkEnumerateInstanceExtensionProperties_t>;
    argsrv_t *argsrv;
    unpack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { }

    ~unpack_marshal_t() {
        if (argsrv->a_2.value) {
          assert(argsrv->a_1.value != 0);
          auto count = argsrv->a_1.ptr()->value;
          assert(count > 0);

          auto Extensions = argsrv->a_2.ptr();

          for (decltype(count) i = 0; i < count; i++) {
            auto Extension = &Extensions[i];

            if (strcmp((const char*)&Extension->extensionName, VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME) == 0) {
              static const char RemovedExtension[] = "VK_KHR_removed_wse";
              memcpy(&Extension->extensionName, RemovedExtension, sizeof(RemovedExtension));
              marshalf("vkEnumerateInstanceExtensionProperties: replaced VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME, %d, %p\n", i, Extension);
            }
          }
        }
    }
};
#endif
#pragma endregion

#include "ldr_ptrs.inl"

ldr_ptrs_t ldr_ptrs;


#include "function_unpacks.inl"

static ExportEntry exports[] = {
    #include "tab_function_unpacks.inl"
    { {}, nullptr }
};

#include "ldr.inl"

EXPORTS(libvulkan)
