#pragma once

#define TRACE_UNPACK_ENTER(name) tracef("enter: unpack, %s\n", name)
#define TRACE_UNPACK_EXIT(name) tracef("exit: unpack, %s\n", name)

#define TRACE_HOSTADDR_UNPACK_ENTER(name) tracef("enter: hostaddr_unpack, %s\n", name)
#define TRACE_HOSTADDR_UNPACK_EXIT(name) tracef("exit: hostaddr_unpack, %s\n", name)


#define TRACE_PACK_ENTER(name) tracef("enter: pack, %s\n", name)
#define TRACE_PACK_EXIT(name) tracef("exit: pack, %s\n", name)

#define TRACE_HOSTADDR_PACK_ENTER(name) tracef("enter: hostaddr_pack, name: %s hostaddr: %p\n", name, (void*)host_addr)
#define TRACE_HOSTADDR_PACK_EXIT(name) tracef("exit: hostaddr_pack, name: %s hostaddr: %p\n", name, (void*)host_addr)

#define TRACE_HOSTCALL_PACK_ENTER(name) tracef("enter: hostcall_pack, name: %s hostaddr: %p\n", name, (void*)host_addr)
#define TRACE_HOSTCALL_PACK_EXIT(name) tracef("exit: hostcall_pack, name: %s hostaddr: %p\n", name, (void*)host_addr)
