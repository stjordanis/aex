# This is tuned for Ubuntu multiarch / multilib + clang + lld

if ("${GUEST_ARCH}" STREQUAL "x86_32")
    set(CMAKE_SYSTEM_PROCESSOR i386)

    # Fun Fact: Ubuntu's i386 actually uses the i686 compiler
    set(triple i386-linux-gnu)
    set(sysroot_triple i686-linux-gnu)
elseif ("${GUEST_ARCH}" STREQUAL "x86_64")
    set(CMAKE_SYSTEM_PROCESSOR x86_64)

    # the triple identifies the target platform
    set(triple x86_64-linux-gnu)
    set(sysroot_triple x86_64-linux-gnu)
else()
    message(FATAL_ERROR "Toolchain doesn't support guest arch: |${GUEST_ARCH}|")
endif()

# Force re-checking of the compiler - cmake sometimes gets confused here and skips it
set(CMAKE_C_ABI_COMPILED False)
set(CMAKE_CXX_ABI_COMPILED False)


# The clang & lld that was used to compile and link FEX is used, in cross compiler mode
#

# It is quite tricky to realiably pass variables to toolchain files, so default to lld here and hope for the best
if (NOT "${LD_OVERRIDE}")
    set(LD_OVERRIDE "lld")
endif()

add_link_options("-fuse-ld=${LD_OVERRIDE}")

set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_CXX_COMPILER_TARGET ${triple})

# Half of this settup is moved to the guest cmake file, as param passimg to toolchain is inconsistent
