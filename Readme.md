# aex-emu - an x86/64 -> arm64 linux usermode emulator
aex-emu allows you to run x86 and x86-64 binaries on an AArch64 host, similar to qemu-user and box86. aex-emu is derived from the FEX-Emu source code.
The repo is getting cleanup / refactored, so considering everything WIP right now.

## Quick start guide

[TBD]

### Navigating the Source
See the [Source Outline](docs/SourceOutline.md) for more information.