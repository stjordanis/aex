#include <FEXCore/Core/SignalDelegator.h>
#include <FEXCore/Utils/LogManager.h>
#include <FEXHeaderUtils/Syscalls.h>
#include <FEXHeaderUtils/ScopedSignalMask.h>

#include <atomic>
#include <unistd.h>
#include <signal.h>
#include <csetjmp>

#include <ucontext.h>

#include "FEXCore/Debug/InternalThreadState.h"

static constexpr auto RAW_SIGSET_SIZE = 8;

#define HOST_DEFER_TRACE do { } while (false)
//#define HOST_DEFER_TRACE do { char str[512]; write(1, str, sprintf(str,"%*s%d %s\n", Previous, "", Previous, __func__)); } while (false)

namespace FEXCore {
  struct ThreadState {
    FEXCore::Core::InternalThreadState *Thread;
    SignalDelegator *HostDeferredDelegator;
    siginfo_t HostDeferredSigInfo;
    sigset_t HostDeferredSigmask; // only 8 bytes used here, depends on kernel configuration
    
    std::atomic<uint16_t> HostDeferredSignalEnabled;
    std::atomic<uint16_t> HostDeferredSignalAutoEnabled;
    std::atomic<int> HostDeferredSignalPending;
  };

  thread_local ThreadState ThreadData{};

  static bool IsSynchronous(int Signal) {
    switch (Signal) {
    case SIGBUS:
    case SIGFPE:
    case SIGILL:
    case SIGSEGV:
    case SIGTRAP:
      return true;
    default: break;
    };
    return false;
  }


  FEXCore::Core::InternalThreadState *SignalDelegator::GetTLSThread() {
    return ThreadData.Thread;
  }

  void SignalDelegator::RegisterTLSState(FEXCore::Core::InternalThreadState *Thread) {
    ThreadData.Thread = Thread;
    RegisterFrontendTLSState(Thread);
  }

  void SignalDelegator::UninstallTLSState(FEXCore::Core::InternalThreadState *Thread) {
    UninstallFrontendTLSState(Thread);
    ThreadData.Thread = nullptr;
  }

  void SignalDelegator::RegisterHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
    SetHostSignalHandler(Signal, Func, Required);
    FrontendRegisterHostSignalHandler(Signal, Func, Required);
  }

  void SignalDelegator::RegisterFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
    SetFrontendHostSignalHandler(Signal, Func, Required);
    FrontendRegisterFrontendHostSignalHandler(Signal, Func, Required);
  }

  void SignalDelegator::SetSignalMask(uint64_t Mask) {
    Mask &= ~ ((1 << (SIGSEGV-1) | (1 << (SIGBUS-1))));

    if (ThreadData.HostDeferredSignalEnabled == 0) {
      ::syscall(SYS_rt_sigprocmask, SIG_SETMASK, &Mask, nullptr, 8);
    } else {
      memcpy(&ThreadData.HostDeferredSigmask, &Mask, sizeof(Mask));
    }
  }

  void SignalDelegator::DeferThreadHostSignals() {
    [[maybe_unused]] auto Previous = ThreadData.HostDeferredSignalEnabled.fetch_add(1, std::memory_order_relaxed);
    LOGMAN_THROW_A_FMT(Previous != UINT16_MAX - 1, "Signal Host Deferring Overflow");
    if (Previous == 0) {
      uint64_t Mask = ~0ULL; // Current linux kernel support up to 64 signals
      ::syscall(SYS_rt_sigprocmask, SIG_SETMASK, &Mask, &ThreadData.HostDeferredSigmask, RAW_SIGSET_SIZE);
    }

    HOST_DEFER_TRACE;
  }

  void SignalDelegator::DeliverThreadHostDeferredSignals() {
    [[maybe_unused]] auto Previous = ThreadData.HostDeferredSignalEnabled.fetch_sub(1, std::memory_order_relaxed);
    LOGMAN_THROW_A_FMT(Previous != 0, "Signal Host Deferring Underflow");

    HOST_DEFER_TRACE;

    if (Previous == 1) {
      if (ThreadData.HostDeferredSignalPending) {
        // this is not meant to work just yet
        ERROR_AND_DIE_FMT("signal error: DeliverThreadHostDeferredSignals actually has pending signals");
        // deliver Pending signal
        {
          // No signals must be delivered in this scope - should be guaranteed.
          // FEX_TODO("Enforce no signal delivery in this scope")
          FHU::ScopedSignalHostDefer hd;
          ucontext_t uctx;
          // It's clearly a guest signal and this ends up being an OS specific issue
          ThreadData.HostDeferredDelegator->HandleGuestSignal(ThreadData.Thread, ThreadData.HostDeferredSignalPending, &ThreadData.HostDeferredSigInfo, &ThreadData.HostDeferredSigmask, nullptr);

          ThreadData.HostDeferredSignalPending = 0;

          syscall(SYS_rt_sigprocmask, SIG_SETMASK, &ThreadData.HostDeferredSigmask, nullptr, RAW_SIGSET_SIZE);
        }
      }
      syscall(SYS_rt_sigprocmask, SIG_SETMASK, &ThreadData.HostDeferredSigmask, nullptr, RAW_SIGSET_SIZE);
    }
  }

  void SignalDelegator::EnterAutoHostDefer() {
    [[maybe_unused]] auto Previous = ThreadData.HostDeferredSignalAutoEnabled.fetch_add(1, std::memory_order_relaxed);
    LOGMAN_THROW_A_FMT(Previous != UINT16_MAX - 1, "Signal Auto Host Deferring Overflow");

    HOST_DEFER_TRACE;
  }

  void SignalDelegator::LeaveAutoHostDefer() {
    [[maybe_unused]] auto Previous = ThreadData.HostDeferredSignalAutoEnabled.fetch_sub(1, std::memory_order_relaxed);
    LOGMAN_THROW_A_FMT(Previous != 0, "Signal Auto Host Deferring Underflow");

    HOST_DEFER_TRACE;
  }

  //FEX_TODO("Enforce that Host Deferred Signals don't get delivered between Acquire/Release")
  bool SignalDelegator::AcquireHostDeferredSignals() {
    if (ThreadData.HostDeferredSignalAutoEnabled.load(std::memory_order_relaxed)) {
      DeferThreadHostSignals();
      return true;
    }

    LOGMAN_THROW_A_FMT(ThreadData.HostDeferredSignalEnabled, "Host Signals need to be Deferred before AcquireHostDeferredSignals");
    return false;
  }

  void SignalDelegator::ReleaseHostDeferredSignals() {
    LOGMAN_THROW_A_FMT(ThreadData.HostDeferredSignalEnabled, "Host Signals need to be Delivered after ReleaseHostDeferredSignals");
    if (ThreadData.HostDeferredSignalAutoEnabled.load(std::memory_order_relaxed)) {
      DeliverThreadHostDeferredSignals();
    }
  }

  void SignalDelegator::HandleSignal(int Signal, void *Info, void *UContext) {

    // Let the host take first stab at handling the signal
    auto Thread = GetTLSThread();

    // Host handling of signals is never deferred
    HostSignalHandler &Handler = HostHandlers[Signal];

    if (!Thread) {
      LogMan::Msg::EFmt("[{}] Thread has received a signal and hasn't registered itself with the delegate! Programming error!", FHU::Syscalls::gettid());
    } else {

      // No signals must be delivered in this scope - should be guaranteed.
      FHU::ScopedSignalHostDefer hd;

      for (auto &Handler : Handler.Handlers) {
        if (Handler(Thread, Signal, Info, UContext)) {
          // If the host handler handled the fault then we can continue now
          return;
        }
      }

      if (Handler.FrontendHandler &&
          Handler.FrontendHandler(Thread, Signal, Info, UContext)) {
        return;
      }
    }

    // Delivery of signal to guest while host code is running may be deferred (Host Deferred Signals)
    if (!ThreadData.HostDeferredSignalEnabled) {
      LOGMAN_THROW_A_FMT(!ThreadData.HostDeferredSignalPending, "Host Deferred signal tearing, delivering {} while pending {}", Signal, ThreadData.HostDeferredSignalPending);
      {
        // No signals must be delivered in this scope - should be guaranteed.
        // FEX_TODO("Enforce no signal delivery in this scope")
        FHU::ScopedSignalHostDefer hd;

        auto _context = (ucontext_t *)UContext;
        // It's clearly a guest signal and this ends up being an OS specific issue
        HandleGuestSignal(Thread, Signal, Info, &_context->uc_sigmask, UContext);
      }
    } else {
      // this is not meant to work just yet
      ERROR_AND_DIE_FMT("signal error: Signal recieved in deffered scope");

      ucontext_t *_context = (ucontext_t *)UContext;
      siginfo_t *_siginfo = (siginfo_t *)Info;

      // signal must be no re-entry here

      [[maybe_unused]] auto Previous = ThreadData.HostDeferredSignalPending.exchange(Signal, std::memory_order_relaxed);

      LOGMAN_THROW_A_FMT(!Previous, "Nested Host Deferred signal, {}", Previous);

      // Store Deferred sigmask
      memcpy(&ThreadData.HostDeferredSigmask, &_context->uc_sigmask, RAW_SIGSET_SIZE);

      // Store Deferred siginfo
      ThreadData.HostDeferredSigInfo = *_siginfo;
      ThreadData.HostDeferredDelegator = this;

      // Block further signals on this thread

      uint64_t sigmask_raw = UINT64_MAX;

      // A special exception is made, SIGSEGV remains enabled during SIGBUS handling for arm64 to handle atomic emulation + SMC cases
      #if defined(_M_ARM_64)
        sigmask_raw &= ~(1ULL << (SIGSEGV - 1));
      #endif

      memcpy(&_context->uc_sigmask, &sigmask_raw, RAW_SIGSET_SIZE);
    }
  }
}
